/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.panneerat.lab3testox;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author hp
 */
public class OXUnitTestBY24 {

    public OXUnitTestBY24() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testCheckWinNoPlayBY_O_output_false() {
        char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
        char currentPlayer = 'O';
        assertEquals(false, TestOX.checkWinner(table, currentPlayer));
    }

    //ชนะแนวนอน O แถว 1
    @Test
    public void testCheckWinRow1BY_O_output_true() {
        char[][] table = {{'O', 'O', 'O'}, {'X', 'X', '-'}, {'-', '-', '-'}};
        char currentPlayer = 'O';
        assertEquals(true, TestOX.checkWinner(table, currentPlayer));
    }

    //ชนะแนวนอน O แถว 2
    @Test
    public void testCheckWinRow2BY_O_output_true() {
        char[][] table = {{'X', 'X', '-'}, {'O', 'O', 'O'}, {'-', '-', '-'}};
        char currentPlayer = 'O';
        assertEquals(true, TestOX.checkWinner(table, currentPlayer));
    }

    //ชนะแนวนอน O แถว 3
    @Test
    public void testCheckWinRow3BY_O_output_true() {
        char[][] table = {{'X', '-', '-'}, {'X', '-', '-'}, {'O', 'O', 'O'}};
        char currentPlayer = 'O';
        assertEquals(true, TestOX.checkWinner(table, currentPlayer));
    }

    //ชนะแนวนอน X แถว 1
    @Test
    public void testCheckWinRow1BY_X_output_true() {
        char[][] table = {{'X', 'X', 'X'}, {'-', '-', '-'}, {'-', 'O', 'O'}};
        char currentPlayer = 'X';
        assertEquals(true, TestOX.checkWinner(table, currentPlayer));
    }

    //ชนะแนวนอน X แถว 2
    @Test
    public void testCheckWinRow2BY_X_output_true() {
        char[][] table = {{'O', '-', '-'}, {'X', 'X', 'X'}, {'-', '-', 'O'}};
        char currentPlayer = 'X';
        assertEquals(true, TestOX.checkWinner(table, currentPlayer));
    }

    //ชนะแนวนอน X แถว 3
    @Test
    public void testCheckWinRow3BY_X_output_true() {
        char[][] table = {{'O', '-', '-'}, {'O', '-', '-'}, {'X', 'X', 'X'}};
        char currentPlayer = 'X';
        assertEquals(true, TestOX.checkWinner(table, currentPlayer));
    }

    //ชนะแนวตั้ง O แถว 1
    @Test
    public void testCheckWinCol1BY_O_output_true() {
        char[][] table = {{'O', '-', '-'}, {'O', '-', '-'}, {'O', 'X', 'X'}};
        char currentPlayer = 'O';
        assertEquals(true, TestOX.checkWinner(table, currentPlayer));
    }

    //ชนะแนวตั้ง O แถว 2
    @Test
    public void testCheckWinCol2BY_O_output_true() {
        char[][] table = {{'X', 'O', '-'}, {'-', 'O', '-'}, {'X', 'O', '-'}};
        char currentPlayer = 'O';
        assertEquals(true, TestOX.checkWinner(table, currentPlayer));
    }

    //ชนะแนวตั้ง O แถว 3
    @Test
    public void testCheckWinCol3BY_O_output_true() {
        char[][] table = {{'X', '-', 'O'}, {'-', '-', 'O'}, {'X', '-', 'O'}};
        char currentPlayer = 'O';
        assertEquals(true, TestOX.checkWinner(table, currentPlayer));
    }

    //ชนะแนวตั้ง X แถว 1
    @Test
    public void testCheckWinCol1BY_X_output_true() {
        char[][] table = {{'X', '-', 'O'}, {'X', '-', 'O'}, {'X', '-', '-'}};
        char currentPlayer = 'X';
        assertEquals(true, TestOX.checkWinner(table, currentPlayer));
    }

    //ชนะแนวตั้ง X แถว 2
    @Test
    public void testCheckWinCol2BY_X_output_true() {
        char[][] table = {{'-', 'X', 'O'}, {'-', 'X', 'O'}, {'-', 'X', '-'}};
        char currentPlayer = 'X';
        assertEquals(true, TestOX.checkWinner(table, currentPlayer));
    }

    //ชนะแนวตั้ง X แถว 3
    @Test
    public void testCheckWinCol3BY_X_output_true() {
        char[][] table = {{'-', 'O', 'X'}, {'-', 'O', 'X'}, {'-', '-', 'X'}};
        char currentPlayer = 'X';
        assertEquals(true, TestOX.checkWinner(table, currentPlayer));
    }

    //O ชนะแนวทแยงซ้าย  \
    @Test
    public void testCheckWinX1_1BY_O_output_true() {
        char[][] table = {{'O', '-', 'X'}, {'-', 'O', 'X'}, {'X', '-', 'O'}};
        char currentPlayer = 'O';
        assertEquals(true, TestOX.checkWinner(table, currentPlayer));
    }

    //O ไม่ชนะแนวทแยงซ้าย \
    @Test
    public void testCheckNoWinX1_2BY_O_output_false() {
        char[][] table = {{'O', 'X', '-'}, {'-', 'O', '-'}, {'O', '-', 'X'}};
        char currentPlayer = 'O';
        assertEquals(false, TestOX.checkWinner(table, currentPlayer));
    }
    
    //X ชนะแนวทแยงซ้าย \
    @Test
    public void testCheckWinX1_1BY_X_output_true() {
        char[][] table = {{'X', '-', '-'}, {'O', 'X', 'O'}, {'-', '-', 'X'}};
        char currentPlayer = 'X';
        assertEquals(true, TestOX.checkWinner(table, currentPlayer));
    }
    
    //X ไม่ชนะแนวทแยงซ้าย \
    @Test
    public void testCheckNoWinX1_2BY_X_output_false() {
        char[][] table = {{'X', '-', '-'}, {'-', 'X', '-'}, {'O', '-', 'O'}};
        char currentPlayer = 'X';
        assertEquals(false, TestOX.checkWinner(table, currentPlayer));
    }
    
    //O ชนะแนวทแยงขวา /
    @Test
    public void testCheckWinX2_1BY_O_output_true() {
        char[][] table = {{'X', 'X', 'O'}, {'-', 'O', '-'}, {'O', '-', 'X'}};
        char currentPlayer = 'O';
        assertEquals(true, TestOX.checkWinner(table, currentPlayer));
    }
    
    //O ไม่ชนะแนวทแยงขวา /
    @Test
    public void testCheckNoWinX2_1BY_O_output_false() {
        char[][] table = {{'X', '-', 'O'}, {'-', 'O', '-'}, {'X', 'O', 'X'}};
        char currentPlayer = 'O';
        assertEquals(false, TestOX.checkWinner(table, currentPlayer));
    }
    
     //X ชนะแนวทแยงขวา /
    @Test
    public void testCheckWinX2_1BY_X_output_true() {
        char[][] table = {{'O', '-', 'X'}, {'O', 'X', '-'}, {'X', '-', '-'}};
        char currentPlayer = 'X';
        assertEquals(true, TestOX.checkWinner(table, currentPlayer));
    }
    
    //X ไม่ชนะแนวทแยงขวา /
    @Test
    public void testCheckNoWinX2_1BY_X_output_false() {
        char[][] table = {{'-', '-', 'O'}, {'-', 'X', 'O'}, {'X', '-', 'X'}};
        char currentPlayer = 'X';
        assertEquals(false, TestOX.checkWinner(table, currentPlayer));
    }
    
       //เสมอกัน 1
    @Test
    public void testCheckWinnerDraw1BY_O_output_true() {
        char[][] table = {{'X', 'X', 'O'}, {'O', 'O', 'X'}, {'X', 'O', 'X'}};
        char currentPlayer = 'O';
        assertEquals(true, TestOX.checkWinner(table, currentPlayer));
    }
    
     //เสมอกัน 2
    @Test
    public void testCheckWinnerDraw2BY_O_output_true() {
        char[][] table = {{'X', 'O', 'X'}, {'X', 'X', 'O'}, {'O', 'X', 'O'}};
        char currentPlayer = 'O';
        assertEquals(true, TestOX.checkWinner(table, currentPlayer));
    }
}
